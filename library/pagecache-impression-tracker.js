jQuery(function($){
	$('a.gofollow').each(function(){
		var tracker = $(this).attr("data-track");
		if (typeof tracker !== 'undefined') {
			impressiontracker_pagecache(tracker);
		}
	});
	$('.g-single noscript').each(function(){
		var $a = jQuery('<div>' + $(this).text() + '</div>').find('a');
		var tracker = $a.attr("data-track");
		if (typeof tracker !== 'undefined') {
			impressiontracker_pagecache(tracker);
		}
	});
});
function impressiontracker_pagecache(tracker) {
	admeta = atob(tracker).split(',');
	var now = Math.round(Date.now()/1000);
	var unixtime = now - admeta[3];

	cookietime = readCookie('adrotate-'+admeta[0]);
	if(cookietime <= unixtime) {
		jQuery.post(
			impression_object_pagecache.ajax_url, 
			{'action': 'adrotate_impression','track': tracker}
		);
		createCookie('adrotate-'+admeta[0], now);
	}
}
function createCookie(name, value) {
    var expires;
    var date = new Date();

    date.setTime(date.getTime() + 86400000);
    expires = "; expires=" + date.toGMTString();
    document.cookie = escape(name) + "=" + escape(value) + expires + "; path=/";
}
function readCookie(name) {
    var nameEQ = escape(name) + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) === 0) return unescape(c.substring(nameEQ.length, c.length));
    }
    return 0;
}