<?php

if ($_POST['action'] == 'adrotate_impression') {
	file_put_contents('process/process.txt', $_POST['track'] . ' ' . adrotate_get_remote_ip() . ' ' . time() . "\n", FILE_APPEND);
}
function adrotate_get_remote_ip(){
	if(empty($_SERVER["HTTP_X_FORWARDED_FOR"])) {
		$remote_ip = $_SERVER["REMOTE_ADDR"];
	} else {
		$remote_ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
	}
	$buffer = explode(',', $remote_ip, 2);

	return $buffer[0];
}